---
title: Exemples Markdown
description: Exemples de syntaxes markdown supportées par ce site
hide:
- navigation
---


# Exemples de syntaxe markdown

Dans ce document, vous trouverez des exemples de syntaxes markdown supportées par ce site pour plus de détails se
référer à la [documentation MkDocs](https://squidfunk.github.io/mkdocs-material/reference/abbreviations/).

!!! warning "Attention"
    Certaines syntaxes ne sont pas standards au format Markdown mais **spécifiques** aux extensions installées sur
    ce site.


## Titres

```markdown
# Titre 1
## Titre 2
### Titre 3
#### Titre 4
##### Titre 5
###### Titre 6
```


## Emphase

```markdown
Italique avec *astérisques* ou _underscores_.

Gras avec **astérisques** ou __underscores__.

Barré avec ~~tildes~~, surligné avec ==Important==, souligné avec ^^Supprimé^^
```

Italique avec *astérisques* ou _underscores_.

Gras avec **double astérisques** ou __double underscores__.

Barré avec ~~double tilde~~, surligné avec ==double égal==, souligné avec ^^double chapeau^^

## Listes

```markdown
Liste ordonnée :

1. Element liste ordonnée 1
2. Element liste ordonnée 2
    1. Sous-element liste ordonnée 1
    2. Sous-element liste ordonnée 2
3. Element liste ordonnée 3  

Liste non ordonnée :

* Element liste non ordonnée
* Element liste non ordonnée
    1. Sous-element liste ordonnée 1
    2. Sous-element liste ordonnée 2
* Element liste non ordonnée
    * Sous-element liste non ordonnée 1
    * Sous-element liste non ordonnée 2
```
Liste ordonnée :

1. Element liste ordonnée 1
2. Element liste ordonnée 2
    1. Sous-element liste ordonnée 1
    2. Sous-element liste ordonnée 2
3. Element liste ordonnée 3  

Liste non ordonnée :

* Element liste non ordonnée
* Element liste non ordonnée
    1. Sous-element liste ordonnée 1
    2. Sous-element liste ordonnée 2
* Element liste non ordonnée
    * Sous-element liste non ordonnée 1
    * Sous-element liste non ordonnée 2


## Liens

There are two ways to create links.

```markdown
[Ceci est un lien](https://inria.fr)

[Ceci est un lien relatif vers l'accueil](../)
```

[Ceci est un lien](https://inria.fr)

[Ceci est un lien relatif vers l'accueil](../)

## Notes et définitions

**Note au passage de la souris :**

```markdown
La signification d'un mot s'affiche au passage de la souris.

*[souris]: Dispositif de pointage pour ordinateur
```

La signification d'un mot s'affiche au passage de la souris.

*[souris]: Dispositif de pointage pour ordinateur

**Note de bas de page :**

```markdown
La signification[^1] d'un mot s'affiche en bas de la page et lors du clic sur le numéro[^2].

[^1]: Définition 1
[^2]: Définition 2
```

La signification[^1] d'un mot s'affiche en bas de la page et lors du clic sur le numéro[^2].

[^1]: Définition 1
[^2]: Définition 2

## Images

```markdown
**Image normale :**

![Texte alternatif](https://picsum.photos/400/250)

**Image flottante à gauche :**

![Texte alternatif](https://picsum.photos/120/100){: align=left }
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

**Image flottante à droite :**

![Texte alternatif](https://picsum.photos/120/120){: align=right }
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

**Image avec légende :**

<figure>
  <img src="https://picsum.photos/600/350" />
  <figcaption>Image centrée avec légende</figcaption>
</figure>
```

**Image normale :**

![Texte alternatif](https://picsum.photos/400/250)

**Image flottante à gauche :**

![Texte alternatif](https://picsum.photos/120/100){: align=left }
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

**Image flottante à droite :**

![Texte alternatif](https://picsum.photos/120/120){: align=right }
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod nulla. Curabitur feugiat, tortor non
consequat finibus, justo purus auctor massa, nec semper lorem quam in massa.

**Image avec légende :**

<figure>
  <img src="https://picsum.photos/600/350" />
  <figcaption>Image centrée avec légende</figcaption>
</figure>


## Code

```
Du code en ligne avec des `apostrophes inversées`
```

Du code en ligne avec des `apostrophes inversées`

Un block de code javascript :

<pre><code>```javascript
var message = 'Hello world';
console.log(message)
```</code></pre>

 ```javascript
 var message = 'Hello world';
 console.log(message)
 ```

Un block de code python :

<pre><code>```python
message = 'Hello world'
print(message)
```</code></pre>

 ```python
 message = 'Hello world'
 print(message)
 ```

## Tableaux

```markdown

| Entete        | Entete        | Entete |
| ------------- |:-------------:| ------:|
| gauche        | centré        | droite |
| blabla        | pif           |    $12 |
| blablabla     | paf           |     $1 |
```

| Entete        | Entete        | Entete |
| ------------- |:-------------:| ------:|
| gauche        | centré        | droite |
| blabla        | pif           |    $12 |
| blablabla     | paf           |     $1 |

## Citations

```markdown
> Ceci est une citation
```

> Ceci est une citation

## Onglets

```
=== "Onglet 1"

    Ceci est note premier onglet

    Il peut cotnenir de nombreux paragraphes

    > Même une citation ou autre block spécial

=== "Onglet 2"

    Ceci est note second onglet

    Il peut cotnenir de nombreux paragraphes

    * Ou
    * même
    * une
    * liste
```

=== "Onglet 1"

    Ceci est note premier onglet

    Il peut cotnenir de nombreux paragraphes

    > Même une citation ou autre block spécial

=== "Onglet 2"

    Ceci est note second onglet

    Il peut cotnenir de nombreux paragraphes

    * Ou
    * même
    * une
    * liste

## Encadrés

**Encadré "à noter" :**

```markdown
!!! note "Titre de la note"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

!!! note "Titre de la note"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

**Encadré dépliable :**

```markdown
??? note "Titre de la note dépliable"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
```

??? note "Titre de la note dépliable"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

**Types d'encadrés :** `info`, `warning`, `success`, `fail`

??? info "Titre"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

??? warning "Titre"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

??? success "Titre"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.

??? fail "Titre"
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla et euismod
    nulla. Curabitur feugiat, tortor non consequat finibus, justo purus auctor
    massa, nec semper lorem quam in massa.
